public with sharing class SomeOtherClass {

    private void test() {
        if(false) return;
    }

    public void complex() {
        if(true) {
            for(Integer x = 0; i < 100; i++) {
                if(x <= 1) {
                    for(Integer y = 0; y < 100; y++) {
                        if(y == x) {
                            for(Integer z = 0; z < 100; z++) {
                                if(z == x && z > y) {
                                    return;
                                }
                            }
                        }
                    }
                } else if(x > 5) {
                    for(Integer y = 0; y < 100; y++) {
                        if(y == x) {
                            for(Integer z = 0; z < 100; z++) {
                                if(z == x && z > y) {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            return;
        }
    }
}

